\documentclass{beamer}
\setbeamertemplate{section in toc}[sections numbered]
\setbeamertemplate{subsection in toc}[subsections numbered]

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{textcomp}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{ stmaryrd }
\usepackage{setspace}
\input{../makros/math_makros.sty}

\setbeamercolor{block title}{bg=gray!30}
\setbeamercolor{block body}{bg=gray!20}

\makeatletter
\setlength{\metropolis@titleseparator@linewidth}{2pt}
\setlength{\metropolis@progressonsectionpage@linewidth}{2pt}
\setlength{\metropolis@progressinheadfoot@linewidth}{2pt}
\makeatother

\title{Algorithmen I Tutorium}
\date{2020-07-16}
\author{Henriette Färber}
\institute{Tutorium 10}

\begin{document}
  \maketitle
  \begin{frame}{Was steht heute an?}
      \tableofcontents
  \end{frame}
\section{Greedy Algorithmen}
\begin{frame}{Was bedeutet \textit{greedy}?}
    \textit{Greedy} bedeutet nichts anderes als \textit{gierig}.
    Im Kontext von Algorithmen bedeutet das, dass wir einmal berechnete
    Ergebnisse stets behalten und nicht mehr verändern.\\[.5em]
    (Wenn man den Begriff so weit strapazieren möchte, kann man 
    \textit{gierig} auch als geizig mit Laufzeit oder Ressourcen 
    auffassen)
\end{frame}
\begin{frame}{Begriffsklärung}
    \vspace{.5em}
    \begin{block}{Greedy Algorithmus}
        Ein Algorithmus wird als \textit{greedy} bezeichnet, wenn er
        in jeder Situation eine lokal optimale Entscheidung trifft.
    \end{block}
    Das bedeutet, dass er zu untersuchende Elemente in einer bestimmten
    Reihenfolge betrachtet und eine Entscheidung, nachdem er sie
    getroffen hat, nicht mehr revidiert.
\end{frame}
\begin{frame}{Beispiele}
    Wir haben im Laufe der Vorlesung bereits einige Greedy Algorithmen
    kennengelernt:
    \pause
    \begin{itemize}
        \item selectionSort
            \pause
        \item Dijkstra
            \pause
        \item Jarník-Prim
            \pause
        \item Kruskal
    \end{itemize}
    Was haben diese Algorithmen gemeinsam?\\
    \pause
    Sie liefern optimale Lösungen.
\end{frame}
\begin{frame}{Aufgabe}
    Dr. Meta hat im Zuge seiner Sparmaßnahmen eine neue Strategie 
    entwickelt: Er lässt seine Minions Passanten das Geld aus der 
    Hosentasche stehlen.\\
    Da dies bei seinen Kollegen als unter der Würde eines Superschurken
    gilt, möchte er bei den gemeinsamen Abendessen in der Cafeteria 
    keinen Verdacht erregen und beschließt, stets mit so wenig Münzen
    wie möglich zu zahlen. (Bsp.: 20ct + 5ct für 25ct Betrag)
\end{frame}
\begin{frame}{Aufgabe}
    Wir nehmen an, dass Dr. Meta stets nur ganzzahlige Werte zahlen muss.
    Welchen wert muss die niedrigstwertige Münze auf jeden Fall haben?
    \\[1em]\pause
    Dr. Meta hat Münzen mit den folgenden Werten dabei: 1ct, 5ct, 10ct,
    50ct. Wie zahlt er am Besten eine Rechnung von 5€ 19ct?\\[1em]\pause
    Wie könnte ein Algorithmus aussehen, der, gegeben einer Menge
    von Münzwerten und einem zu zahlenden Betrag, diese minimale
    Menge von Münzen findet? Wir gehen dabei davon aus, dass Dr. Meta 
    eine unendliche Menge jedes Münzwertes angehäuft hat.
\end{frame}
\begin{frame}{Aufgabe}
    \begin{itemize}
        \item gegeben: $m_1,\ldots,m_k$ Münzwerte mit 
            \begin{itemize}
                \item $m_1 = 1$
                \item $\forall i \in \{1,\ldots, k-1\}: m_i < m_{i+1}$
            \end{itemize}
        \item außerdem: der zu zahlende Betrag $K \in \mathbb{N}_0$
    \end{itemize}
    \pause
    Idee:
    \begin{enumerate}
        \item Bestimme das größte $m_i$ so, dass $m_i \leq K$
        \item Setze $K = K - m_i$, merke $m_i$
        \item Falls K = 0, kehre zurück, ansonsten beginne wieder bei 1
    \end{enumerate}
    \pause
    \textbf{Behauptung}: Dieser Algorithmus liefert ein optimales 
    Ergebnis.
\end{frame}
\begin{frame}{Optimalitätsbeweis}
    \begin{block}{Minimalität}
        Wir nennen eine Menge M von Münzen, die zusammen den Wert K 
        bilden, minimal, wenn sie für alle $i \in \{1,\ldots, k-1\}$ 
        weniger als $\frac{m_{i+1}}{m_i}$ Münzen vom Wert $m_i$ enthält.
    \end{block}
    Wenn eine Münzmenge also nach dieser Definition minimal ist, gibt
    es keine anderen mengen, die ebenfalls Betrag K bilden, aber weniger
    Münzen beinhalten.
\end{frame}
\begin{frame}{Optimalitätsbeweis}
    Seien $m_1,\ldots,m_k$ die Menge an möglichen Münzwerten und $i\in
    \{1,\ldots,k\}$. Sei M eine Menge von Münzen, die keine Münzen von 
    Wert $m_i$ oder höher enthalte. Der maximale Wert von M ist
    \begin{gather*}
    (\frac{m_2}{m_1} - 1)\cdot m_1 + (\frac{m_3}{m_2} - 1)\cdot m_2 +
        \dotsb + (\frac{m_i}{m_{i-1}} - 1)\cdot m_{i-1})\\
        = (m_2 - m_1) + (m_3 - m_2) + \dotsb + (m_i - m_{i-1})\\
        = m_i - m_1\\
        = m_i - 1
    \end{gather*}
\end{frame}
\begin{frame}{Optimalitätsbeweis}
    Wir beweisen nun die Optimalität des Algorithmus, den wir vorher
    skizziert haben, indem wir beweisen, dass er immer die richtigen
    Münzen auswählt:\\[.5em]
    Wir betrachten nun K mit $m_i \leq K < m_{i+1}$. Eine optimale Menge
    M' muss im Sinne der vorigen Definition minimal sein.\\
    Wegen $m_i \leq K$ muss M' mindestens eine Münze von Wert $m_i$ oder
    höher enthalten. Wegen $K < m_{i+1}$ kann S keine Münze mit Wert 
    $m_{i+1}$ oder höher enthalten. Somit enthält M' also eine Münze
    mit Wert $m_i$.\\[.5em]
    Dieses Argument lässt sich sukzessive fortsetzen, indem wir nun 
    eine Münze mit Wert $m_i$ aus  M' entfernen und $K = K - m_i$ setzen.
    Damit finden wir die Münzen einer optimalen Lösung in genau der
    Reihenfolge, in der sie der Algorithmus hinzufügt.
\end{frame}
\begin{frame}[t]{Coin Change Problem}
    Hurra! Dr. Meta kann also nun weiterhin seine Minions auf Passanten
    loslassen, ohne sich vor seinen Kollegen zu blamieren.\\
    Fragt sich noch: Wie schnell kann Dr. Meta seine minimale
    Münzmenge finden, sobald er einen Preis erfährt?
\end{frame}
\section{Dynamic Programming}
\begin{frame}{Motivation}
    Das Coin Change Problem können wir also mit einem Greedy 
    Algorithmus effizient und optimal lösen. Es gibt allerdings viele 
    Probleme, bei denen dies nicht möglich ist, wie zum Beispiel das
    allseits beliebte Rucksackproblem.\\
    In der Regel handelt es sich dabei um Optimierungsprobleme.
    \begin{block}{Optimierungsproblem}
        Unter einem Optimierungsproblem verstehen wir ein Problem, bei
        dem eine bestimmte Eigenschaft einer Lösung minimiert bzw. 
        maximiert werden und unter allen möglichen Lösungen die
        optimale Lösung ausgewählt werden soll.
    \end{block}
\end{frame}
\begin{frame}{Optimalitätsprinzip}
    Viele Optimierungsprobleme folgen dem
    \begin{block}{Optimalitätsprinzip}
        Eine optimale Lösung einer Probleminstanz ist zusammengesetzt 
        aus optimalen Lösungen von Teilproblemen.\\
        Besitzt ein Teilproblem mehrere optimale Lösungen, kann eine 
        beliebige gewählt werden.
    \end{block}
    Daraus ergibt sich bereits die Idee der Dynamischen 
    Programmierung:\\
    Wir erstellen eine große Tabelle, in der wir optimale Lösungen
    von Teilproblemen speichern und setzen "größere" Lösungen aus
    "kleineren" zusammen.
\end{frame}
\begin{frame}{Ansätze}
    Wir wollen also eine solche Tabelle anlegen. Dazu können wir 
    (müssen wir allerdings nicht, es gibt auch andere Umsetzungen)
    zwischen zwei gängigen Ansätzen wählen:
    \begin{itemize}
        \item \textbf{bottom-up}\\
            Beginne das Ausfüllen der Tabelle bei den kleinstmöglichen
            Teilproblemen und baue diese iterativ zusammen
        \item \textbf{top-down}\\
            Steige rekursiv in Teilprobleme ab, bis ein Basisfall
            erreicht ist und nutze die Tabelle zum Speichern bereits
            errechneter Lösungen
    \end{itemize}
\end{frame}
\begin{frame}{Erinnerung an Knapsack}
    Das omnipräsente Optimierungsproblem in Algorithmen I ist das 
    Knapsack- oder auch Rucksackproblem:
    \begin{itemize}
        \item gegeben:
            \begin{itemize}
                \item Menge K von Gegenständen
                \item Kostenfunktion $c: K \rightarrow \mathbb{R}$
                \item Gewichtsfunktion $w: K \rightarrow \mathbb{Z}$
                \item Maximalgewicht $W \in \mathbb{Z}$
            \end{itemize}
        \item gesucht: Menge $L \subset K$ so, dass w(L) $\leq$ W und
            c(L) maximal
    \end{itemize}
\end{frame}
\begin{frame}{Aufgabe}
    Woraus besteht eine Lösung für das Rucksackproblem?\\ \pause
    Aus einer Teilmenge $M \subset K$\\[1em]
    Welche Lösungen sind für dieses Problem gültig?\\ \pause
    Alle Lösungen, die die Gewichtsbeschränkung einhalten.\\[1em]
    Welche Lösungen sind optimal?\\ \pause
    Diejenigen gültigen Lösungen mit maximierten Kosten (bzw. Profit,
    aus Sicht des Schmugglers)
\end{frame}
\begin{frame}{Coin Change Problem II}
    Wir wandeln nun das vorhin betrachtete Coin Change Problem etwas
    ab:
    \begin{itemize}
        \item gegeben:
            \begin{itemize}
                \item Münzwerte: W := $w_1,\ldots,w_N \in \mathbb{N}$ 
                    paarweise verschieden
                \item $K \in \mathbb{N}$
            \end{itemize}
        \item gesucht: X := $(x_1,\ldots,x_N)$ mit
            \begin{itemize}
                \item $\forall i \in \{1,\ldots,N\}: x_i \in \mathbb{N}$
                \item $x_i$ bezeichnet die Anzahl der Münzen mit Wert 
                    $w_i$\\[.5em]
                \item w(X) := $\sum_{i = 1}^N{(x_i \cdot w_i)}$ = K
                    \\[.5em]
                \item c(X) := $\sum_{i = 1}^N{x_i}$ minimal
            \end{itemize}
    \end{itemize}
\end{frame}
\begin{frame}{Coin Change Problem II}
    Dieses Coin Change Problem können wir mit Hilfe von 
    Dynamischer Programmierung lösen. Dazu müssen wir zunächst eine
    Struktur innerhalb des Problems finden, das dem Optimalitätsprinzip
    genügt, damit wir wissen, was wir überhaupt in die Tabelle 
    schreiben.\\
    Wodurch ist diese Struktur gegeben?
\end{frame}
\begin{frame}{Coin Change Problem II}
    Sei I eine feste Instanz des Coin Change Problems.
    Entfernen wir eine beliebige Münze mit Wert $w_i$ aus einer 
    optimalen Lösung M für I, erhalten wir eine neue Lösung M' mit 
    w(M') = w(M) - $w_i$und c(M') = c(M) - 1.\\[1em]
    Angenommen, M' wäre keine optimale Lösung für die Instanz I' mit 
    K' = w(S) - $w_i$. Dann existiert eine andere optimale Lösung M'' mit
    w(M'') = w(S) - $w_i$ und c(M'') $<$ c(M) - 1. Dann liefert M'' durch
    Hinzunahme einer Münze mit Wert $w_i$ eine leichtere optimale Lösung
    für I, als M es ist.\\
    $\lightning$ Widerspruch zu: M ist optimal für I
\end{frame}
\begin{frame}{Coin Change Problem II}
    Mit diesem Wissen können wir nun die folgende Rekurrenz aufstellen:\\
    Sein c(k), $k \in \mathbb{N}$ die minimal nötige Anzahl von Münzen,
    um den Wert k zu bilden. Wir setzen:
      \begin{align*}
      Sei\ T(n)=
          \begin{cases}
              \infty  &, k < 0\\
              0 &, k = 0\\
              1 + min_{i = 1,\ldots,N}(c(k - w_i)) &, k > 0\\
          \end{cases}
      \end{align*}
      Wir merken uns zusätzlich zu jedem Wert k den Wert der letzten 
      Münze, die in der Rekursion benutzt wurde um c(k) zu erzielen.
      \\[.5em]
      \textbf{Frage}: Handelt es sich bei diesem Vorgehen um einen 
      buttom-up oder um einen top-down Ansatz?
\end{frame}
\begin{frame}{Coin Change Problem II - Beispiel}
    Sei W = (1,2,4,5) und K = 7. Wir füllen die Tabelle:
    \begin{center}
        \begin{tabular}{ c || c | c | c | c | c | c | c | c | c }
            \textbf{k} & $<0$ & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7\\
            \hline
            \textbf{c(k)} & $\infty$ & 0 & & & & & & &\\
            \hline
            \textbf{p(k)} & $\bot$ & $\bot$ & & & & & & & 
        \end{tabular}
    \end{center}
\end{frame}
\begin{frame}{Coin Change Problem II - Beispiel}
    Sei W = (1,2,4,5) und K = 7. Wir füllen die Tabelle:
    \begin{center}
        \begin{tabular}{ c || c | c | c | c | c | c | c | c | c }
            \textbf{k} & $<0$ & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7\\
            \hline
            \textbf{c(k)} & $\infty$ & 0 & 1 & & & & & &\\
            \hline
            \textbf{p(k)} & $\bot$ & $\bot$ & 1 & & & & & & 
        \end{tabular}
    \end{center}
\end{frame}
\begin{frame}{Coin Change Problem II - Beispiel}
    Sei W = (1,2,4,5) und K = 7. Wir füllen die Tabelle:
    \begin{center}
        \begin{tabular}{ c || c | c | c | c | c | c | c | c | c }
            \textbf{k} & $<0$ & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7\\
            \hline
            \textbf{c(k)} & $\infty$ & 0 & 1 & 2 & & & & &\\
            \hline
            \textbf{p(k)} & $\bot$ & $\bot$ & 1 & 1 & & & & & 
        \end{tabular}
    \end{center}
\end{frame}
\begin{frame}{Coin Change Problem II - Beispiel}
    Sei W = (1,3,4,5) und K = 7. Wir füllen die Tabelle:
    \begin{center}
        \begin{tabular}{ c || c | c | c | c | c | c | c | c | c }
            \textbf{k} & $<0$ & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7\\
            \hline
            \textbf{c(k)} & $\infty$ & 0 & 1 & 2 & 1 & & & &\\
            \hline
            \textbf{p(k)} & $\bot$ & $\bot$ & 1 & 1 & 3 & & & & 
        \end{tabular}
    \end{center}
\end{frame}
\begin{frame}{Coin Change Problem II - Beispiel}
    Sei W = (1,2,4,5) und K = 7. Wir füllen die Tabelle:
    \begin{center}
        \begin{tabular}{ c || c | c | c | c | c | c | c | c | c }
            \textbf{k} & $<0$ & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7\\
            \hline
            \textbf{c(k)} & $\infty$ & 0 & 1 & 2 & 1 & 1 & & &\\
            \hline
            \textbf{p(k)} & $\bot$ & $\bot$ & 1 & 1 & 3 & 4 & & & 
        \end{tabular}
    \end{center}
\end{frame}
\begin{frame}{Coin Change Problem II - Beispiel}
    Sei W = (1,2,4,5) und K = 7. Wir füllen die Tabelle:
    \begin{center}
        \begin{tabular}{ c || c | c | c | c | c | c | c | c | c }
            \textbf{k} & $<0$ & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7\\
            \hline
            \textbf{c(k)} & $\infty$ & 0 & 1 & 2 & 1 & 1 & 1 & &\\
            \hline
            \textbf{p(k)} & $\bot$ & $\bot$ & 1 & 1 & 3 & 4 & 5 & & 
        \end{tabular}
    \end{center}
\end{frame}
\begin{frame}{Coin Change Problem II - Beispiel}
    Sei W = (1,2,4,5) und K = 7. Wir füllen die Tabelle:
    \begin{center}
        \begin{tabular}{ c || c | c | c | c | c | c | c | c | c }
            \textbf{k} & $<0$ & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7\\
            \hline
            \textbf{c(k)} & $\infty$ & 0 & 1 & 2 & 1 & 1 & 1 & 2 &\\
            \hline
            \textbf{p(k)} & $\bot$ & $\bot$ & 1 & 1 & 3 & 4 & 5 & 1 & 
        \end{tabular}
    \end{center}
\end{frame}
\begin{frame}{Coin Change Problem II - Beispiel}
    Sei W = (1,2,4,5) und K = 7. Wir füllen die Tabelle:
    \begin{center}
        \begin{tabular}{ c || c | c | c | c | c | c | c | c | c }
            \textbf{k} & $<0$ & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7\\
            \hline
            \textbf{c(k)} & $\infty$ & 0 & 1 & 2 & 1 & 1 & 1 & 2 & 2\\
            \hline
            \textbf{p(k)} & $\bot$ & $\bot$ & 1 & 1 & 3 & 4 & 5 & 1 & 3 
        \end{tabular}
    \end{center}
\end{frame}
\begin{frame}{Aufgabe}
    Sei nun W = (1,5,6,7) und K = 10.\\[1em] 
    Füllt die eben gezeigte Tabelle für diese Instanz von Coin Change 
    aus!
\end{frame}
\begin{frame}[t]{Lösung}
    Erinnerung: W = (1,5,6,7) und K = 10
\end{frame}
\begin{frame}{Pseudocode}
    \begin{algorithm}[H]
        \begin{algorithmic}[1]
            \scriptsize
            \Function{coinChange}{W: [\N], K: \NZer}: \NZer $\cup$ -1
            \State c \< $<0,\infty,\ldots,\infty>$: [\NZer $\cup$ \inft,
            K]
            \State p \< $<0,\bot,\ldots,\bot>$: [\N $\cup \bot$, K]
            \State \textbf{return} solve(K, W, c, p)
            \EndFunction
        \end{algorithmic}
    \end{algorithm}
    \begin{algorithm}[H]
        \begin{algorithmic}[1]
            \scriptsize
            \Function{solve}{K: \NZer, W: [\N], c: [\NZer $\cup$
            \inft], p: [\N $\cup \bot$] }: \NZer $\cup$ -1
            \State cMin \< compute(K, W, c, p)
            \If{cMin = \inft } \textbf{return} -1
            \EndIf
            \State s \< $<0,\ldots,0>$: [\NZer]
            \State cRem \< K
            \While{cRem $>$ 0}
            \State k \< p[cRem]
            \State cRem \< cRem - W[k]
            \State s[k] \< s[k] + 1
            \EndWhile
            \State \textbf{return} s
            \EndFunction
        \end{algorithmic}
    \end{algorithm}
\end{frame}
\begin{frame}{Pseudocode}
    \begin{algorithm}[H]
        \begin{algorithmic}[1]
            \scriptsize
            \Function{compute}{k: \NZer, N: \N, W: [\N, N], c: [\NZer $\cup$
            \inft], p: [\N $\cup \bot$] }: \NZer $\cup$ \inft
            \If{k $<$ 0} \textbf{return} \inft
            \EndIf
            \If{c[k] $\neq \bot$} \textbf{return} c[k]
            \EndIf
            \State m \< \inft, l \< $\bot$
            \For{i \< 1 \textbf{to} N}
            \If{ 1 + compute(k - W[i]) $<$ m }
            \State m \< 1 + compute(k - W[i]), l \< i
            \EndIf
            \EndFor
            \State c[k] \< m, p[k] \< l
            \State \textbf{return} c[k]
            \EndFunction
        \end{algorithmic}
    \end{algorithm}
    Laufzeit: \pause \bigO{N \cdot K} 
\end{frame}
\begin{frame}{Aufgabe}
    Der Tag der Entscheidung rückt näher! Dr. Meta bereitet sich auf
    das große Rennen der Schurkenwissenschaftsweltmeisterschaften vor, 
    indem er am Rande der Rennstrecke entlang Minions positioniert, 
    die seinen manchmal recht behäbigen SUV anschieben, um ihm ein 
    bisschen Extraantrieb zu geben. Natürlich können diese Minions nur
    in Winkeln stehen, wo ihre \textit{Hilfe} nicht zu sehen ist.
    Außerdem muss Dr. Meta Acht geben, nicht durch zu viel Beschleunigung
    auf zu kurzer Strecke Verdächtigungen auf sich zu ziehen. Er
    beschließt daher, nachdem er einmal angeschoben wurde, mindestens bis
    zum übernächsten Minion zu warten, bis er sich wieder \textit{ein
    wenig helfen} lässt.
\end{frame}
\begin{frame}{Aufgabe}
    Nun drängt die Zeit! Die Minions sind schon fröhlich und völlig
    willkürlich an verschiedene Positionen gehüpft, sie können nicht mehr
    umsortiert werden.\\
    Die Minions sind unterschiedlich stark und können Dr. Meta 
    unterschiedlich vile zusätzliche Beschleunigung geben. Da die Minions
    natürlich von Dr.Meta gechipt wurden, weiß er, welcher Minion wo 
    steht.\\[1em]
    Wie kann Dr. Meta mit Hilfe von Dynamischer Programmierung die
    Minions ermitteln, von denen er sich anschieben lassen sollte, 
    um die größtmögliche Zeitersparnisse zu erhalten?
\end{frame}
\begin{frame}{Aufgabe}
    Modellierung von Dr. Metas Problem:
    \begin{itemize}
        \item gegeben: 
            \begin{itemize}
                \item Minion-Menge M := ($m_1,\ldots, m_k$)
                \item Zeitersparnisfunktion c: M $\rightarrow$ \NZer
            \end{itemize}
        \item gesucht: Teilmenge M* von M mit maximaler Gesamtersparnis,
            nach den vorher beschriebenen Regeln erstellt
    \end{itemize}
    Gebt eine Rekursion an, die für die Implementierung eines 
    Algorithmus zur Lösung von Dr. Metas Problem geeignet ist!
\end{frame}
\begin{frame}{Lösung}
    Eine geeignete Rekursion ist:\\[1em]
    saved[i] = max\{saved[i + 1], saved[i + 2] + c($m_i$)\}
\end{frame}
\section{Lineare Programmierung}
\begin{frame}{Lineare Optimierungsprobleme}
    Wir haben bereits reguläre Optimierungsprobleme kennengelernt. 
    Nun schränken wir uns auf lineare Optimierungsprobleme ein.
    \begin{block}{Lineares Optimierungsproblem}
        Ein lineares Optimierungsproblem besteht aus:
        \begin{itemize}
            \item einer lineare Zielfunktion
            \item linearen Nebenbedingungen (Restriktionen)
            \item der Aufgabe, den Wert der Zielfunktion zu maximieren
                oder minimieren
        \end{itemize}
    \end{block}
\end{frame}
\begin{frame}{Notation}
    Wir notieren ein lineares Optimierungsproblem in der folgenden Form:
    \begin{itemize}
        \item gegeben: 
            \begin{itemize}
                \item A $\in \mathbb{R}^{m \times n}$
                \item c $\in \mathbb{R}^{n}$
                \item b $\in \mathbb{R}^{m}$ 
                \item x $\in \mathbb{R}^{n}$, den Vektor mit 
                    Entscheidungsvariablen
            \end{itemize}
        \item gesucht: Belegungen von Komponenten von x so, dass 
            \begin{itemize}
                \item $c \cdot x$ optimal
                \item $\forall j \in \{1,\ldots,m\}: \sum_{i = 1}^n{
                    (a_{ij} \cdot x_i)} \leq b_j$
            \end{itemize}
    \end{itemize}
\end{frame}
\begin{frame}{Integer Linear Programming}
    Beim Integer Linear Programming stellen sich uns Probleme mit der
    gleichen Struktur, aber mit dem Unterschied, dass eine Lösung nur
    dann gültig ist, wenn alle Belegungen der Entscheidungsvariablen
    ganzzahlig sind, d.h. wenn x $\in \mathbb{Z}^{n}$.
\end{frame}
\begin{frame}{Anmerkung}
    Wir werden uns in Algorithmen I nicht mit dem Lösen von Problemen
    mit Hilfe von (Integer) Linear Programming beschäftigen, sondern nur
    mit dem Aufstellen der Probleme.\\[1em]
    Für diejenigen, die gerne mehr erfahren wollen, bietet sich das
    Nebenfach Operations Research an. Aus dem Lehrbuch, dass von 
    S. Nickel, O. Stein und K. Waldmann zu der Einführungsveranstaltung
    verfasst wurde\\ (Operations Research, 2. Auflage, Springer, Berlin 
    Heidelberg) stammt auch das folgende Beispiel.
\end{frame}
\begin{frame}{Modellierung}
    Betrachtet die folgende Situation: Ein Unternehmen stellt zwei 
    Produkte her: $P_1 und P_2$, wobei $P_1$ 3€ Gewinn pro produzierter
    Mengeneinheit (ME) bringt und $P_2$ sogar 4€.\\
    Ziel des Unternehmens ist es, möglichst viel Gewinn zu machen. Dabei
    sind die Ressourcenverbrauche der Produkte zu beachten, die sich 
    wie folgt zusammensetzen:
    \begin{center}
        \begin{tabular}{c | c | c}
            \textbf{Ressource} & $P_1$ & $P_2$\\
            \hline \hline
            \textbf{Maschinenstunden} & 3h & 2h\\
            \hline
            \textbf{Rohstoff} & 5ME & 10ME\\
            \hline
            \textbf{Arbeitsstunden} & 0h & 0.5h
        \end{tabular}
    \end{center}
    Im betrachteten Zeitraum stehen nur 1200 Maschinenstunden, 3000ME
    Rohstoffe und 125 Arbeitsstunden zur Verfügung.
\end{frame}
\begin{frame}{Modellierung}
    Wir benötigen zwei Entscheidungsvariablen, d.h. x = $(x_1\ x_2)$,
    wobei $x_i$ die Menge bezeichnet, die von $P_i$ produziert werden 
    soll. Es gilt x $\in \mathbb{Z}^2$.\\
    Es handelt sich um ein Maximierungsproblem. Maximiert werden soll der
    Gewinn, der sich aus der Summe der Gewinne für die beiden Produkte
    zusammensetzt. Damit ist die Zielfunktion gegeben als\\[0.5em]
    $f(x) = 3x_1 + 4x_2$.\\[.5em]
\end{frame}
\begin{frame}{Modellierung}
    Die Restriktionen ergeben sich aus den Ressourceneinschränkungen:\\
    \begin{gather*}
        3x_1 + 2x_2 \leq 1200\\
        5x_1 + 10x_2 \leq 3000\\
        0.5x_2 \leq 125\\
        x_1, x_2 \geq 0
    \end{gather*}
    Die letzte Restriktion, die sogenannten Nichtnegativitätsbedingungen,
    sind wichtig und dürfen nicht vergessen werden!
\end{frame}
\begin{frame}{Aufgabe}
    Dr. Meta lässt für die Konstruktion seiner schurkenhaften Maschinen
    zwei Gruben, in denen seine Minions Erz abbauen.\\
    Es werden drei verschiedene Erzarten gefördert: feines, mittleres und
    grobes Erz.\\
    Um seine Produktion stets aufecht erhalten zu können, benötigt Dr.
    Meta pro Woche mindestens 12t grob-, 8t mittel- und 24t feinkörniges
    Erz.\\
    Die Betriebskosten belaufen sich für Grube 1 auf 20000€ und bei 
    Grube 2 auf 1600€. In Grube 1 werden pro Tag 6t grob- 2t mittel- und
    4t feinkörniges Erz gefördert, Grube 2 bringt täglich 2t grob-, 2t
    mittel- und 12t feinkörniges Erz.\\[.5em]
    Formuliert ein lineares Optimierungsproblemproblem, welches diese
    Situation modelliert!
\end{frame}
\begin{frame}{Lösung}
    \begin{itemize}
        \item Entscheidungsvariablen = \pause
            Anzahl der Arbeitstage in den 
            Gruben $\rightsquigarrow x = (x_1\ x_2) \in \mathbb{Z}$
        \item Zielfunktion: \pause $f(x) = 20000x_1 + 16000x_2$
        \item wir wollen die Zielfunktion \pause minimieren
        \item Restriktionen:
    \end{itemize}
    \pause
    \begin{gather*}
        6x_1 + 2x_2 \leq 12\\
        2x_1 + 2_2 \leq 8\\
        4x_1 + 12x_2 \leq 24\\
        x_1, x_2 \leq 7\\
        x_1, x_2 \geq 0
    \end{gather*}
\end{frame}
  \begin{frame}{Fragen?}
      Fragen!
  \end{frame}
  \begin{frame}
      \begin{figure}[htb]
          \includegraphics[width=1\textwidth]{../XKCD/efficiency.png}
          \caption{\href{https://xkcd.com/1445/}{https://xkcd.com/1445/}}
      \end{figure}
  \end{frame}
  \end{document}
