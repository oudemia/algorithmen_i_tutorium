\section{$\mathcal{O}$-Kalkül}

\begin{frame}{Wiederholung}
    Das $\mathcal{O}$-Kalkül sollte euch bereits aus GBI bekannt sein.
    \\[1em]
    \pause
    \begin{block}{Das $\mathcal{O}$-Kalkül...}
        ... gibt das \textbf{asymptotische} Verhalten einer Funktion 
        bzw. eines Algorithmus an.
    \end{block}
    Es geht dabei nur um eine grobe Abschätzung, welche Operationen 
    unabhängig von ihrer konkreten Darstellung betrachtet.
\end{frame}

\begin{frame}{Landau-Symbole}
    Sei $f:\mathbb{N}\rightarrow\mathbb{R\textsuperscript{+}}$ eine 
    Funktion und $n \in \mathbb{N}$.
    Wir definieren:\\[.5em]
    \begin{block}{Komplexitätsklassen}
        \pause
        $\mathcal{O}(f(n))$=\\
        \quad $\{g:\mathbb{N}\rightarrow\mathbb{R\textsuperscript{+}}\ 
        |\ \exists c > 0\ \exists n\textsubscript{0} \in \mathbb{N}\ 
         \forall n \geq n\textsubscript{0}:g(n)\leq c*f(n)\}$\\[0.5em]
        \pause
        $\Omega(f(n))$=\\
        \quad $\{g:\mathbb{N}\rightarrow\mathbb{R\textsuperscript{+}}\ 
        |\ \exists c > 0\ \exists n\textsubscript{0} \in \mathbb{N}\ 
        \forall n \geq n\textsubscript{0}:g(n)\geq c*f(n)\}$\\[0.5em]
        \pause
        $\Theta(f(n))=\mathcal{O}(f(n))\cap\Omega(f(n))$\\[0.5em]
        \pause
        $o(f(n))$=\\
        \quad $\{g:\mathbb{N}\rightarrow\mathbb{R\textsuperscript{+}}\ 
        |\ \forall c > 0\ \exists n\textsubscript{0} \in \mathbb{N}\ 
        \forall n \geq n\textsubscript{0}:g(n)\leq c*f(n)\}$\\[0.5em]
        \pause
        $\omega(f(n))$=\\
        \quad $\{g:\mathbb{N}\rightarrow\mathbb{R\textsuperscript{+}}\ 
        |\ \forall c > 0\ \exists n\textsubscript{0} \in \mathbb{N}\ 
        \forall n \geq n\textsubscript{0}:g(n)\geq c*f(n)\}$
    \end{block}
\end{frame}

\begin{frame}{Landau-Symbole}
    Seien $f, g:\mathbb{N}\rightarrow\mathbb{R\textsuperscript{+}}$ 
    Funktionen und $n \in \mathbb{N}$. Wir definieren:\\[1em]
    \begin{block}{In Worten:}
        \begin{itemize}
            \item[] $g \in \mathcal{O}(f(n))\rightsquigarrow$ g wächst 
                höchstens so schnell wie f
            \item[] $g \in \Omega(f(n))\rightsquigarrow$ g wächst 
                mindestens so schnell wie f
            \item[] $g \in \Theta(f(n))\rightsquigarrow$ g wächst genau 
                so schnell wie f
            \item[] $g \in o(f(n))\rightsquigarrow$ g wächst echt 
                langsamer als f
            \item[] $g \in \omega(f(n))\rightsquigarrow$ g wächst echt 
                schneller als f
        \end{itemize}
    \end{block}
    \pause
    \begin{alertblock}{Achtung!}
        Die Schreibweise ist $f(n) \in \mathcal{O}(g(n))$, NICHT $f(n)=\mathcal{O}(g(n))$!
    \end{alertblock}
\end{frame}

\begin{frame}{Landau-Symbole}
    \begin{block}{Also ist g(n) in...}
        \begin{itemize}
            \item[...] $\mathcal{O}(f(n))\ \Leftrightarrow\ 0 \leq 
                \lim\limits_{n \rightarrow \infty}{\left| 
                \frac{g(n)}{f(n)}\right|} < \infty$\\[1em]
            \item[...] $o(f(n))\ \Leftrightarrow\ \lim\limits_{n 
                \rightarrow \infty}{\left| \frac{g(n)}{f(n)}\right|}=0$
                \\[1em]
            \item[...] $\Omega(f(n))\ \Leftrightarrow\ 0 < 
                \lim\limits_{n \rightarrow \infty}{\left| 
                \frac{g(n)}{f(n)}\right|} \leq \infty$\\[1em]
            \item[...] $\omega(f(n))\ \Leftrightarrow\ \lim\limits_{n 
                \rightarrow \infty}{\left| \frac{g(n)}{f(n)}\right|} = 
                \infty$\\[1em]
            \item[...] $\Theta(f(n))\ \Leftrightarrow\ 0 < 
                \lim\limits_{n \rightarrow \infty}{\left| 
                \frac{g(n)}{f(n)}\right|} < \infty$\\[1em]
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}{Rechnen im $\mathcal{O}$-Kalkül}
    Im $\mathcal{O}$-Kalkül ist nur die \textbf{dominante}, d.h. die am
    stärksten steigende \textbf{Komponente} einer Funktion relevant.
    \\[1em]
    Konstante Faktoren und additive Konstanten werden nicht beachtet.
    \\
    Auch die Basen von Logarithmen vernachlässigen wir.\\[1em]

    \pause
    So ist zum Beispiel $42n^2+12 \in \Theta(n^2)$.
\end{frame}
\begin{frame}{Aufgabe}
    Welche der folgenden Aussagen treffen zu?
    \begin{enumerate}
        \item $\sqrt{n} \in \mathcal{O}(log\ n)$
        \item $10+n^2 \in o(2^n)$
        \item $log_2 n \in \omega(log\textsubscript{3} n)$
        \item $n\textsuperscript{n-1} \in \mathcal{O}(n^n)$
        \item $f(n)+g(n) \in \mathcal{O}(max\{f(n), g(n)\})$
        \item $ 2^n \in o(n!)$
    \end{enumerate}
\end{frame}
\begin{frame}[t]{Lösung}
    \alert{Falsch: $\sqrt{n} \in \mathcal{O}(log\ n)$}\\[2em]
    $\lim\limits_{n \rightarrow \infty}{\sqrt{n}} = \infty\ \land\ 
    \lim\limits_{n \rightarrow \infty}{log\ n} = \infty\ \Rightarrow$
    l'Hopital anwenden\\[1em]
    \[
    \lim\limits_{n \rightarrow \infty}{\left| \frac{\sqrt{n}}{log\ n}
    \right|} = 
    \lim\limits_{n \rightarrow \infty}{\left| \frac{\frac{1}{2*\sqrt{n}}}
    {\frac{1}{n}}\right|} =
    \lim\limits_{n \rightarrow \infty}{\left| \frac{n}{2 * \sqrt{n}}
    \right|} = 
    \lim\limits_{n \rightarrow \infty}{\left| \frac{n * \sqrt{n}}
    {2 * n}\right|} =
    \]
    \[
    \lim\limits_{n \rightarrow \infty}{\left| \frac{1}{2} * \sqrt{n}
    \right|} = \infty
    \]
    \\[2em]
    $\Rightarrow \sqrt{n} \in \omega(log\ n)$
\end{frame}

\begin{frame}[t]{Lösung}
    Wahr: $10+n^2 \in o(2^n)$\\[2em]
    $\lim\limits_{n \rightarrow \infty}{10 + n^2} = \infty\ \land\ 
    \lim\limits_{n \rightarrow \infty}{2^n} = \infty\ \Rightarrow$
    l'Hopital anwenden\\[1em]
    \[
    \lim\limits_{n \rightarrow \infty}{\left| \frac{10 + n^2}{2^n}
    \right|} = 
    \lim\limits_{n \rightarrow \infty}{\left| \frac{2 * n}{log(2) *
    2^n}\right|} =^{l'Hopital}
    \lim\limits_{n \rightarrow \infty}{\left| \frac{1}{log^2(2) * 2^n}
    \right|} = 0
    \]
\end{frame}

\begin{frame}[t]{Lösung}
    \alert{Falsch: $log_2 n \in \omega(log\textsubscript{3} n)$}\\[2em]
    \[
        \log_3 n = \frac{\log_2 n}{\log_2 3} = \frac{1}{\log_2 3} *
        \log_2 n
    \]\\[1em]
    $ \frac{1}{\log_2 3}$ ist ein konstanter Faktor, also:\\[1em]
    $\log_2 n \in \Theta(\log_3 n)$
\end{frame}

\begin{frame}[t]{Lösung}
    Wahr: $n\textsuperscript{n-1} \in \mathcal{O}(n^n)$\\[2em]
    \[
        \frac{n^{n-1}}{n} = \frac{1}{n} \xrightarrow{n \to \infty} 0
    \]
\end{frame}

\begin{frame}[t]{Lösung}
    Wahr: $f(n)+g(n) \in \mathcal{O}(max\{f(n), g(n)\})$\\[2em]
    Sei $max := max\{f(n), g(n)\}$. Es ist zu zeigen, dass 
    \[
        \exists c > 0\ \exists n\textsubscript{0} \in \mathbb{N}\ 
        \forall n \geq n\textsubscript{0}: f(n) + g(n)\leq max
    \]
    \\[0.5em]
    Nach der Definition des Maximums gilt:
    \[ \forall n \in \mathbb{N}: f(n) \leq max \land g(n) \leq max
    \]
    \[
        \Rightarrow \forall n \in \mathbb{N}:  f(n) + g(n) \leq 2 * max
    \]
    \\[.5em]
    $\Rightarrow$ Wähle c = 2 und $n_{0}$ = 1.
\end{frame}

\begin{frame}[t]{Lösung}
    Wahr: $ 2^n \in o(n!)$\\[2em]
    \[
        \frac{2^n}{n!} = \frac{2 * 2 * ... * 2 * 2}{n * (n-1) * ... * 2 
        * 1}
    \]
    \\[1em]
    Sowohl im Nenner als auch im Zähler stehen n Faktoren, d.h.
    \[
        \frac{2^n}{n!} = \frac{2}{n} * \frac{2}{n-1} * ... * 1 * 2
        \xrightarrow{n \to \infty} 0
    \]
\end{frame}
