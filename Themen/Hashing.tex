\documentclass{beamer}
\usetheme{metropolis}
\usepackage{textcomp}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{color, colortbl}

\definecolor{Gray}{gray}{0.9}

\setbeamercolor{block title}{bg=gray!30}
\setbeamercolor{block body}{bg=gray!20}

\title{Algorithmen I Tutorium}
\date{\today}
\author{Henriette Färber}
\institute{Tutorium 10}

\begin{document}
  \maketitle
  \section{Hashing}
  \begin{frame}{Motivation}
      Zurück zu Dr. Meta und seiner Liste mit Todfeinden. Wir haben im 
      letzten Tutorium bereits festgestellt, dass er besser eine Liste 
      als ein Array nutzen sollte.\\[2em]
      Wenn Dr. Meta jedoch überprüfen will, ob er seinen Mitarbeiter Obda
      schon auf die Liste gesetzt hat, muss er lineare Zeit aufwenden,
      um die Liste zu durchsuchen. Und die Liste ist \textit{lang}.\\[2em]
      Dr. Meta ist empört!
  \end{frame}
  \begin{frame}{Motivation II}
      Wir wollen eine Datenstruktur aufbauen, die die Operationen
      \begin{itemize}
          \item find(k: Key)
          \item put(e: Element)
          \item remove(k: Key)
      \end{itemize}
      in erwartet linearer Zeit ausführen kann.\\[2em]
      \pause
      Vorhang auf für...
  \end{frame}
  \section{Hashtabellen}
  \begin{frame}{Was ist Hashing?}
      Der Begriff "hashing" kommt aus dem Englischen und bedeutet etwa 
      "zerhacken". Wir zerhacken die Gesamtheit unseres Inputs in kleine
      Teile, die jeweils über einen Schlüssel adressierbar sind.\\[1em]
      \pause
      Wir wollen also nun ein Element e in eine Hashtabelle eintragen, 
      mitsamt des eindeutigen Schlüssels k, über den wir dann auf e 
      zugreifen können. Dazu müssen wir Folgendes leisten:
      \pause
      \begin{enumerate}
          \item den Hash h(k) berechnen
          \item e an der Stelle einfügen, die h(k) in der Hashtabelle 
              zugeordnet ist
          \item Kollisionsauflösung, sollte sich bereits ein anderes 
              Element an dieser Stelle befinden
      \end{enumerate}
  \end{frame}
  \begin{frame}{Wichtige Anmerkungen}
      \begin{itemize}
          \item $\mathcal{O}(1)$ erreichen wir nur, wenn wir eine "gute"
              Hashfunktion wählen
              \begin{itemize}
                  \item[$\rightsquigarrow$] möglichst keine Kollisionen:
                      jedes Element wird auf einen anderen Wert 
                      abgebildet
                  \item[$\rightsquigarrow$] schlecht gewählte Hashtabelle
                      $\rightarrow \mathcal{O}(n)$ (worst case)
              \end{itemize}
          \item oft ist das Element selbst der Schlüssel (z.B. bei 
              reellen Zahlen)
          \item h(key) muss einen \textit{eindeutigen} Wert entgegennehmen     
      \end{itemize}
  \end{frame}
  \begin{frame}{Hashfunktionen}
      \begin{block}{Definition: Hashfunktion}
          $h:\ \mathbb{K} \rightarrow \mathbb{M}$\\
          Dabei ist $\mathbb{K}$ eine Schlüsselmenge und $\mathbb{M}$ 
          eine endliche Menge der möglichen Hashwerte.
          Es gilt $|\mathbb{K}| \geq |\mathbb{M}|$.
      \end{block}
      \pause
      Welche Kriterien erfüllt eine "gute" Hashfunktion?
      \pause
      \begin{itemize}
          \item Surjektivität: alle Hashwerte können genutzt werden
          \item Gleichverteilung: alle Hashwerte werden mit der gleichen
              Wahrscheinlichkeit getroffen
          \item Kollisionen sind möglichst selten
      \end{itemize}
  \end{frame}
  \begin{frame}{Aufgabe}
      Gegeben seien die beiden Hashfunktionen $h_1(k)= k\ mod\ 11$ und 
      $h_2(k)= k\ mod\ 4$.\\[1em]
      Welche von beiden ist besser geeignet, um die Elemente\\ 
      (4, 8, 10, 12, 16, 22, 24, 28)\\ 
      möglichst gleichmäßig zu verteilen?
  \end{frame}
  \begin{frame}{Auswahl einer geeigneten Hashfunktion}
      Problem: In der Praxis können wir in der Regel keine Aussagen über
      die Menge an Schlüsseln machen, die wir der Hashtabelle übergeben.\\
      Entscheiden wir und für eine feste Hashfunktion, ist es allein 
      abhängig von der Schlüsselmenge, wie (un-)gleichmäßig die Elemente
      verteilt werden.
      \\[1em]
      Frage: Wie können wir trotzdem eine möglichst günstige Hashfunktion
      wählen?
  \end{frame}
  \begin{frame}{Idee der zufällig gewählten Hashfunktion}
    Lösung: Wir wählen unsere Hashfunktion aus einer Familie (geeigneter)
    Hashfunktionen zufällig.\\[1em]
    Natürlich kann diese Funktion immer noch ungünstig für unsere 
    späteren Eingaben sein. Der Vorteil ist jedoch, dass die 
    Wahrscheinlichkeit dafür sinkt.
  \end{frame}
  \begin{frame}{Universelles Hashing}
      \begin{block}{Definition: Universelle Hashfamilie}
          Sei $\mathcal{H}$ eine Menge von Hashfunktionen, welche die 
          Schlüsselmenge $\mathbb{K}$ auf $\mathbb{M} = \{0, ..,\ m-1\}$
          abbliden.\\
          $\mathcal{H}$ heißt \textbf{universell}, wenn für ein zufällig
          gewähltes $h \in \mathcal{H}$ gilt:\\[1em]
          $\forall k,\ l \in \mathbb{K},\ k \neq l:\ 
          \mathbb{P}[h(k) = h(l)] = \frac{1}{m}$\\[1em]
          Anders formuliert: Wählen wir eine Hashfunktion $h \in 
          \mathcal{H}$ zufällig, ist die Kollisionswahrscheinlichkeit
          für zwei beliebige Schlüssel $\frac{1}{m}$.
      \end{block}
      \pause
      \begin{block}{Beispiel einer universellen Hashfamilie}
          Wenn $\mathbb{K} \subseteq \mathbb{N}$:\\
          $h_{a,b}(k)=((ak + b)\ mod\ p)$\\
          wobei $a, b \in \mathbb{N}_0,\ a
          \neq 0$ und p ist eine Primzahl.
      \end{block}
  \end{frame}
  \section{Kollisionsauflösung}
  \begin{frame}{Kollisionen}
      \begin{block}{Definition: Kollision}
          Eine Kollision liegt vor, wenn zwei verschiedene Schlüssel auf 
          den gleichen Hash abgebildet werden:\\
          $k, l \in \mathbb{K}:\ h(k)=h(l)$, aber $k \neq l$
      \end{block}
      Auch die beste Hashfunktion kann uns nicht vor Kollisionen schützen.\\
      Wir lernen deswegen zwei verschiedene Strategien zur 
      Kollisionsauflöung kennen: verkettete Listen und lineare Suche.
  \end{frame}
  \begin{frame}{Kollisionsauflösung mit verketteten Listen}
      Idee:
      \begin{itemize}
          \item Jeder Hashtabelleneintrag enthält eine einfach verkettete
              Liste
          \item Kommt es zu einer Kollision, wird das neue Element 
              einfach an das ende der Liste eingefügt.
      \end{itemize}
  \end{frame}
  \begin{frame}{Visualisierung}
      $h(k) = k\ mod\ 5$\\[2em]
      (1, 10, 3, 16) $\rightsquigarrow$
      \begin{figure}[htb]
          \includegraphics[width=1\textwidth]{../illustrations/hashmap_01.pdf}
      \end{figure}
  \end{frame}
  \begin{frame}{Visualisierung}
      $h(k) = k\ mod\ 5$\\[2em]
      (10, 3, 16) $\rightsquigarrow$
      \begin{figure}[htb]
          \includegraphics[width=1\textwidth]{../illustrations/hashmap_02.pdf}
      \end{figure}
  \end{frame}
  \begin{frame}{Visualisierung}
      $h(k) = k\ mod\ 5$\\[2em]
      (3, 16) $\rightsquigarrow$
      \begin{figure}[htb]
          \includegraphics[width=1\textwidth]{../illustrations/hashmap_03.pdf}
      \end{figure}
  \end{frame}
  \begin{frame}{Visualisierung}
      $h(k) = k\ mod\ 5$\\[2em]
      (16) $\rightsquigarrow$
      \begin{figure}[htb]
          \includegraphics[width=1\textwidth]{../illustrations/hashmap_04.pdf}
      \end{figure}
  \end{frame}
  \begin{frame}{Visualisierung}
      $h(k) = k\ mod\ 5$\\[2em]
      () $\rightsquigarrow$
      \begin{figure}[htb]
          \includegraphics[width=1\textwidth]{../illustrations/hashmap_05.pdf}
      \end{figure}
  \end{frame}
  \begin{frame}{Aufgabe}
      Gegeben sei eine Hashtabelle mit der Hashfunktion $h(k) = k\ mod\ 
      7$.\\
      Skizziert die Hashtabelle und fügt die folgenden Elemente ein,
      wobei Ihr verkettete Listen zur Kollisionsauflösung verwendet:\\
      (12, 2, 8, 9, 25, 16, 21, 30, 14, 3)\\[2em]
      Was ergeben sich für Probleme, wenn die Hashfunktion ungünstig 
      gewählt wurde?
  \end{frame}
  \begin{frame}{Lösung}
      \begin{figure}[htb]
          \includegraphics[width=1\textwidth]{../illustrations/solution_hashmap_01.pdf}
      \end{figure}
  \end{frame}
  \begin{frame}{Kollisionsauflösung mit Linear Probing}
      Idee: die freien Plätze in der Hashtabelle selbst nutzen\\[2em]
      insert(e):
      \begin{itemize}
          \item Elemente werden direkt in die Hashtabelle eingefügt
          \item Kommt es zur Kollision, wird das neue Element e auf dem
              nächsten freien Platz rechts von der besetzten Stelle
              eingefügt ("Linear Probing")
      \end{itemize}
      \pause
      Frage: Was passiert, wenn die Hashfunktion ungünstig gewählt wurde?
  \end{frame}
  \begin{frame}{Kollisionsauflösung mit Linear Probing}
      find(k):
      \begin{itemize}
          \item Der Hash von k wird berechnet und zunächst die 
              zugehörige Stelle in der Hashtabelle überprüft
          \item Ist die Stelle bereits von einem anderen Element 
              besetzt, wird nach rechts weitergelaufen, bis zu das 
              gesuchte Element gefunden wird
          \item Das gesuchte Element ist nicht in der Hashtabelle
              enthalten, wenn die Suche auf eine leere Stelle oder 
              auf diejenige trifft, bei der sie begonnen hat.
      \end{itemize}
      \pause
      Frage: Warum kann an dieser Stelle die Suche abgebrochen werden?
  \end{frame}
  \begin{frame}{Kollisionsauflösung mit Linear Probing}
      remove(k):
      \begin{itemize}
          \item Das zu löschende Element wird gesucht und, wenn 
              vorhanden, entfernt.
          \item Nun wird die Liste von rechts nach links durchlaufen und
              die Elemente, die durch die frei gewordene Stelle nach 
              vorne rücken können, nach links verschoben.
      \end{itemize}
  \end{frame}
  \begin{frame}{Visualisierung}
      $h(k) = k\ mod\ 5$\\[2em]
      (1, 10, 3, 16) $\rightsquigarrow$
      \begin{figure}[htb]
          \includegraphics[width=1\textwidth]{../illustrations/hashmap_01.pdf}
      \end{figure}
  \end{frame}
  \begin{frame}{Visualisierung}
      $h(k) = k\ mod\ 5$\\[2em]
      (10, 3, 16) $\rightsquigarrow$
      \begin{figure}[htb]
          \includegraphics[width=1\textwidth]{../illustrations/hashmap_02.pdf}
      \end{figure}
  \end{frame}
  \begin{frame}{Visualisierung}
      $h(k) = k\ mod\ 5$\\[2em]
      (3, 16) $\rightsquigarrow$
      \begin{figure}[htb]
          \includegraphics[width=1\textwidth]{../illustrations/hashmap_03.pdf}
      \end{figure}
  \end{frame}
  \begin{frame}{Visualisierung}
      $h(k) = k\ mod\ 5$\\[2em]
      (16) $\rightsquigarrow$
      \begin{figure}[htb]
          \includegraphics[width=1\textwidth]{../illustrations/hashmap_04.pdf}
      \end{figure}
  \end{frame}
  \begin{frame}{Visualisierung}
      $h(k) = k\ mod\ 5$\\[2em]
      () $\rightsquigarrow$
      \begin{figure}[htb]
          \includegraphics[width=1\textwidth]{../illustrations/hashmap_15.pdf}
      \end{figure}
  \end{frame}
  \begin{frame}{Aufgabe}
      Gegeben sei eine Hashtabelle mit der Hashfunktion $h(k) = k\ mod\ 
      9$.\\
      Skizziert die Hashtabelle und fügt die folgenden Elemente ein,
      wobei Ihr Linear Probing zur Kollisionsauflösung verwendet:\\
      (0, 18, 2, 21, 42, 24, 32, 81)\\[2em]
  \end{frame}
  \begin{frame}{Lösung I}
      \begin{figure}[htb]
          \includegraphics[width=1\textwidth]{../illustrations/solution_hashmap_11.pdf}
      \end{figure}
      \pause
      Löscht nun die folgenden Elemente aus der Hashtabelle:
      (0, 2, 42).
  \end{frame}
  \begin{frame}{Lösung II}
      \begin{figure}[htb]
          \includegraphics[width=1\textwidth]{../illustrations/solution_hashmap_12.pdf}
      \end{figure}
  \end{frame}
  \begin{frame}{Vergleich der Laufzeiten}
      Laufzeiten mit \textbf{verketteten Listen}:\\[1em]
      \begin{center}
          \begin{tabular}{c | c | c } 
              operation & average case & worst case\\[0.5em] 
              \hline
              \rowcolor{Gray}
              find & $\mathcal{O}(1)$ &  $\mathcal{O}(n)$\\ 
              put & $\mathcal{O}(1)$ &  $\mathcal{O}(1)$\\ 
              \rowcolor{Gray}
              remove & $\mathcal{O}(1)$ &  $\mathcal{O}(n)$\\ 
          \end{tabular}
          \end{center}
          \vspace{2em}
          Laufzeiten mit \textbf{Linear Probing}:
          \begin{center}
              \begin{tabular}{c | c | c } 
                  operation & average case & worst case\\[0.5em] 
                  \hline
                  \rowcolor{Gray}
                  find & $\mathcal{O}(1)$ &  $\mathcal{O}(n)$\\ 
                  put & $\mathcal{O}(1)$ &  $\mathcal{O}(n)$\\ 
                  \rowcolor{Gray}
                  remove & $\mathcal{O}(1)$ &  $\mathcal{O}(n)$\\ 
              \end{tabular}
              \end{center}
          \end{frame}
          \end{document}
