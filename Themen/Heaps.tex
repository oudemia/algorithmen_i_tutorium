\documentclass{beamer}
\usetheme{metropolis}
\usepackage{textcomp}
\usepackage{algorithm}
\usepackage{algpseudocode}

\setbeamercolor{block title}{bg=gray!30}
\setbeamercolor{block body}{bg=gray!20}

\newcommand{\N}{$\mathbb{N}$}
\newcommand{\<}{$\gets$}

\title{Algorithmen I Tutorium}
\date{\today}
\author{Henriette Färber}
\institute{Tutorium 10}

\begin{document}
  \maketitle
  \section{Heaps}
  \begin{frame}{Präambel}
      Im Rahmen der Vorlesung beschränken wir uns auf Binary Heaps. Dabei
      sollten wir jedoch im Hinterkopf behalten, dass es auch andere
      Ausprägungen von Heaps gibt.\\[3em]
      Im Rahmen des Tutoriums beschränke ich mich auf Min-Heaps. Das dient
      der Vereinfachung und verhindert, dass ich Alles doppelt erzähle. 
      Max-Heaps werden intuitiv analog zu Min-Heaps aufgebaut und müssen 
      nicht explizit erklärt werden.
  \end{frame}
  \begin{frame}{Was ist nun ein Heap?}
      Ein Heap kann als Baum visualisiert werden. In unserem Fall 
      betrachten wir also "Binärbäume". Die Knoten eines Heaps enthalten
      vergleichbare Elemente. Für diese gilt:\\[2em]
      \begin{block}{Die Heap-Invariante}
          Für alle Eltern-Elemente und ihre zugehörigen Kinder gilt:\\
          parent $\leq$ child
      \end{block}
      \vspace{2em}
      Daraus folgt direkt, dass das minimale Element in einem Heap stets
      die Wurzel darstellt.
  \end{frame}
  \begin{frame}{Motivation}
      Ausgehend von der Heap-Invariante lässt sich erahnen, dass Heaps sich
      besonders für Priority Queues eigenen (Element mit kleinstem Wert 
      $\widehat{=}$ Element mit höchster Priorität).\\
      Weiterhin werden wir den HeapSort-Algorithmus kennenlernen, der 
      auf der Heap-Struktur basiert.\\[1em]
      \pause
      Ein Heap soll die folgenden Operationen effizient unterstützen:
      \begin{itemize}
          \item \textsc{heapify}: erstellt einen Heap mit n Elementen
          \item \textsc{insert} und \textsc{remove}
          \item \textsc{min} und \textsc{deleteMin}
          \item \textsc{decreaseKey}: Verringern des Wertes eines Elements
      \end{itemize}
      \pause
      Wollen wir eine Datenstruktur, in der wir Elemente (effizient) 
      suchen können, sind Heaps nicht das richtige.
  \end{frame}
  \begin{frame}{Implizite Baumdarstellung}
      Die Operationen, die wir uns von einem Heap wünschen, müssen 
      garantieren, dass die Invariante auch nach ihrer Ausführung gilt.
      Da diese Operationen aber nicht auf einer Baumstruktur, sondern 
      auf einem Array arbeiten, müssen wir uns erst überlegen, wie wir 
      dieses als Binärbaum interpretieren können.
      \begin{figure}[htb]
          \includegraphics[width=0.9\textwidth]{../illustrations/heaps/heap.pdf}
      \end{figure}
  \end{frame}
  \begin{frame}{Implizite Baumdarstellung}

      Für einen Binärbaum der Höhe h brauchen wir ein Array mit $2^h$ 
      Einträgen. Alle Knoten werden über ihren Index im Array indiziert:
      \begin{itemize}
          \item der Wunzelknoten hat Index 1
          \item der linke Nachfolger des Knotens i befindet sich an Index
              2i
          \item der rechte Nachfolger des Knotens i befindet sich an 
              Index 2i+1
          \item Der Index des Eltern-Knotens von Knoten i ist $\lfloor 
              \frac{i}{2} \rfloor$
      \end{itemize}
      \begin{figure}[htb]
          \includegraphics[width=0.7\textwidth]{../illustrations/heaps/heap.pdf}
      \end{figure}
  \end{frame}
  \begin{frame}{Elemente im Baum anheben und absenken}
      Damit wir beliebige Elemente in unseren Heap einfügen und aus 
      unserem Heap löschen können, benötigen wir \textsc{siftDown}, um 
      ein Element auf eine tiefere Ebene abzusenken und \textsc{siftUp}, 
      um es auf eine höhere Ebene zu heben.
  \end{frame}
  \begin{frame}{siftDown}
      \begin{algorithm}[H]
          \begin{algorithmic}[1]
              \Procedure{siftDown}{array: [element, n], i: \N}
              \If{2i $<$ n}
              \\
              \Comment wir wählen dem kleineren der Nachfolger von i 
              \If{$(2i+1 > n)\ \vee \ array[2i] \leq array[2i+1]$}
              \State m \< 2i: \N
              \Else
              \State m \< 2i+1: \N
              \EndIf
              \If{$array[i] > array[m]$}
              \Comment Invariante verletzt
              \State swap(array[i], array[m])
              \State siftDown(array, m)
              \EndIf
              \EndIf
              \EndProcedure
          \end{algorithmic}
      \end{algorithm}
  \end{frame}
  \begin{frame}{siftUp}
      \begin{algorithm}[H]
          \begin{algorithmic}[1]
              \Procedure{siftUp}{array: [element, n], i: \N}
              \If{$i = 1\ \vee \ array[\lfloor \frac{i}{2} \rfloor] < 
              array[i]$}
              \\
              \Comment Invariante nicht verletzt
              \State \textbf{return}
              \EndIf
              \State swap(array[i], $array[\lfloor \frac{i}{2} \rfloor]$)
              \State siftUp($\lfloor \frac{i}{2} \rfloor$)
              \EndProcedure
          \end{algorithmic}
      \end{algorithm}
  \end{frame}
  \begin{frame}{Heapify}
      Da wir nun wissen, wie wir Elemente umsortieren können, können wir 
      uns darüber Gedanken machen, wie wir aus einem Array einen Heap 
      machen.\\[1em]
      1. Idee: wir starten mit einem leeren Heap und füllen die Elemente
      vom Array in den Heap um.\\
      $\rightsquigarrow \ \mathcal{O}(n* \log n)$\\[1em]
      Aber es geht noch besser!\\[1em]
      \pause
      Wir betrachten das Eingabearray einfach als Heap, in dem die 
      Invariante (an mehreren Stellen) verletzt ist. Nun kümmern wir uns 
      noch darum, die Gültigkeit der Invariante wieder herzustellen.
  \end{frame}
  \begin{frame}{Heapify}
      Bezeichne 0 die unterste Ebene des Baumes. Alle darüberliegenden 
      Ebenen werden aufsteigend nummeriert.\\[1em]
      Beobachtung: Gilt die Heap-Invariante für alle Teilbäume, deren 
      Wurzel auf einer bestimmten Ebene liegt, so können wir die 
      Invariante auf der darüberliegenden Ebene herstellen, indem wir 
      \textsc{siftDown} auf die Wurzeln aufrufen.\\[1em]
      Idee: Wir stellen die Invariante ebenenweise durch wiederholtes 
      Aufrufen von siftDown wieder her.
  \end{frame}
  \begin{frame}{Heapify}
      \begin{algorithm}[H]
          \begin{algorithmic}[1]
              \Procedure{heapify}{array: [element, n]}
              \For{$i \gets \lfloor \frac{n}{2} \rfloor$ \textbf{to} 1}
              \State siftDown(array, i)
              \EndFor
              \EndProcedure
          \end{algorithmic}
      \end{algorithm}
      \vspace{3em}
      Frage: Warum startet die Schleife bei $\lfloor \frac{n}{2} \rfloor$
      statt bei n?\\
      \pause
      Wir überspringen die Blätter.
  \end{frame}
  \begin{frame}{Laufzeit von Heapify}
      Sei l eine Ebene in unserem Heap mit n Einträgen. Für die Anzahl 
      k an Knoten auf dieser Ebene gilt:\\[1em]
      $k \leq \frac{2^{(\log n)}}{2^l} \leq \frac{n}{2^l}$\\[1em]
      Da wir siftDown auf allen Ebenen bis auf der untersten aufrufen, 
      können wir die Laufzeit wie folgt abschätzen:\\
      \[
          \mathcal{O}(\displaystyle\sum_{l = 0}^{\lfloor \log n \rfloor}
          \frac{n}{2^l}*l) = \mathcal{O}(\displaystyle\sum_{l = 0}^
          {\lfloor \log n \rfloor}\frac{l}{2^l}*n) = \mathcal{O}
          (\displaystyle\sum_{l = 0}^{\infty}\frac{l}{2^l}*n)
  \]
  Diese Laufzeit konvergiert wiederum gegen $\mathcal{O}(n)$
  \end{frame}
  \begin{frame}{Aufgabe}
      Wir haben nun eine Idee, wie Heaps funktionieren. Anbetracht 
      dessen, was wir gerade über \textsc{heapify} gelernt haben:
      Was leisten die folgenden Operationen?
      \begin{itemize}
          \item \textsc{decreaseKey}
          \item \textsc{min}, \textsc{deleteMin}
          \item \textsc{insert}
          \item \textsc{remove}
      \end{itemize}
  \end{frame}
  \begin{frame}{Laufzeiten}
      \begin{itemize}
          \item \textsc{heapify}: $\mathcal{O}(n)$
          \item \textsc{min}: $\mathcal{O}(1)$
          \item \textsc{insert}: $\mathcal{O}(\log n)$
          \item \textsc{deleteMin}: $\mathcal{O}(\log n)$
          \item \textsc{decreaseKey}: $\mathcal{O}(\log n)$
      \end{itemize}
  \end{frame}
  \begin{frame}{Aufgabe}
      Gegeben sei das folgende Array: $<3, 5, 7, 5, 42, 8, 10, 11, 9>$
      \\[2em]
      \begin{itemize}
          \item Handelt es sich um eine gültige implizite Darstellung
              eines Binärheaps?
              \pause
          \item Stellt das Array in Form eins Binärbaums dar.
          \item Fügt die Elemente 4 und 2 in den Heap ein.
          \item Löscht das Element 3.
      \end{itemize}
  \end{frame}
  \begin{frame}{Lösung}
  \end{frame}
  \begin{frame}{Lösung}
  \end{frame}
  \begin{frame}{Lösung}
  \end{frame}
\end{document}
