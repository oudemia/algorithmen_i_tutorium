\documentclass{beamer}
\usetheme{metropolis}
\usepackage{textcomp}
\usepackage{algorithm}
\usepackage{algpseudocode}
\input{../makros/math_makros.sty}

\setbeamercolor{block title}{bg=gray!30}
\setbeamercolor{block body}{bg=gray!20}

\title{Algorithmen I Tutorium}
\date{\today}
\author{Henriette Färber}
\institute{Tutorium 10}

\begin{document}
\maketitle
\section{Union-Find}
\begin{frame}{Motivation}
    Wir benötigen (unter anderem für einen der folgenden Algorithmen)
    eine Datenstruktur, die wir für die Repräsentation von Partitionen,
    d.h. disjunkten Mengen $V_1, \ldots, V_k$ innerhalb einer Menge V 
    so, dass\\[.5em]
    $\forall i,j \in \{1, \ldots, k\}: V_i \cap V_j = \emptyset$ und\\
    $\bigcup_{i = 1}^k V_i = V$\\[.5em]
    Insbesondere soll diese Datenstruktur die folgenden Operationen
    unterstützen:
    \begin{itemize}
        \item find(v) mit v $\in$ V\\
            In welchem $V_i, i \in \{1,\ldots,k\}$ liegt v?
        \item union($V_i, V_j$) mit i $\neq$ j\\
            Verschmelzen von $V_i$ und $V_j$
    \end{itemize}
\end{frame}
\begin{frame}{Idee}
    Wir weisen jeder Teilmenge $V_i$ ein einzelnes Element v $\in$ V 
    als parent zu. $V_i$ ist dann eindeutig über sein v 
    identifizierbar, deswegen nennen wir v auch den Repräsentanten von
    $V_i$.\\[.5em]
    Wollen wir dann überprüfen, ob sich w $\in$ V in der Teilmenge 
    $V_i$ befindet, müssen wir feststellen, ob w in der gleichen Menge
    wie v liegt. Das heißt wir brauchen ein Verfahren, um von einem 
    Element auf den Repräsentanten seiner Teilmenge schließen zu 
    können.\\[0.5em]
    Ein Vorteil: Wollen wir zwei Mengen $V_i$ und $V_j$ verschmelzen, 
    kann das darüber geschehen, dass wir den beiden einen gemeinsamen
    Repräsentanten zuweisen. Eine der Teilmengen übernimmt also den
    Repräsentanten der anderen.
\end{frame}
\begin{frame}{Idee}
    Wir wollen die Teilmengen als gewurzelte Bäume speichern. In diesen
    hat jedes Element einen Zeiger auf sein Eltern-Element. Zunächst
    bauen wir diese naiv auf.\\[.5em]
    Die Ausgangssituation sei stets einelementige Teilmengen. Jedes 
    Element hat also seine eigene Teilmenge und fungiert als initiales
    Element als Wurzel und Repräsentant seiner Teilmenge.\\[.5em]
    Unsere Operationen setzten wir nun wie folgt um:
    \pause
    \begin{itemize}
        \item find(v): laufe den Baum von v entlang bis zur Wurzel und 
            gib dieses Wurzel-Element zurück
        \item union(v,w): setze den Eltern-Zeiger der Wurzel v der einen
            Teilmenge auf die Wurzel w der anderen Teilmenge
    \end{itemize}
\end{frame}
\begin{frame}{Idee}
    \begin{figure}[htb]
        \includegraphics[width=.7\textwidth]{../illustrations/union-find/min-exmpl.pdf}
    \end{figure}
\end{frame}
\begin{frame}{Idee}
    \begin{figure}[htb]
        \includegraphics[width=.7\textwidth]{../illustrations/union-find/min-exmpl01.pdf}
    \end{figure}
\end{frame}
\begin{frame}{Aufgabe}
    Zum Speichern des aktuellen Zustands benötigen wir lediglich ein
    Array parent: [\NZer, n] für \norm{V} = n.\\[1em]
    Formuliert basierend auf dieser Repräsentation die Operationen find
    und union in Pseudocode!\\[1em]
    Welche Laufzeit haben diese Operationen?
\end{frame}
\begin{frame}{Lösung}
    \begin{algorithm}[H]
        \begin{algorithmic}[1]
            \Function{find}{v: \NZer}: \NZer
            \If{parent[v] = v}
            \State \textbf{return} v
            \EndIf
            \State \textbf{return} find(parent[v])
            \EndFunction
        \end{algorithmic}
    \end{algorithm}
    \pause
    \begin{algorithm}[H]
        \begin{algorithmic}[1]
            \Procedure{union}{v,w: \NZer}
            \If{find(v) = find(w)}
            \State parent[find(v)] \< find(w)
            \EndIf
            \EndProcedure
        \end{algorithmic}
    \end{algorithm}
\end{frame}
\begin{frame}{Probleme}
    Die Laufzeit von union und find hängt von der Balanciertheit der
    Baumstrukturen ab.\\[0.5em]
    Wenn wir diese naive Implementierung verwenden, kann uns unsere
    Baumstruktur durch eine Serie von ungünstigen union-Ausführungen
    zu einer linearen Liste entarten. Damit hätten beide Operationen
    lineare Laufzeit.\\[0.5em]
    Damit geben wir uns nicht zufrieden und führen zwei Optimierungen ein:
    \begin{itemize}
        \item Union by Rank
        \item Pfadkompression
    \end{itemize}
\end{frame}
\begin{frame}{Union-by-Rank}
    Ziel ist es, die Höhe eines Baumes so weit wie möglich zu 
    beschränken. Dazu führen wir das Konzept des Ranges ein und 
    verwenden es beim Verschmelzen von Teilmengen:
    \begin{itemize}
        \item anfänglich hat jede Teilmenge Rang 0
        \item beim Verschmelzen unterschieden wir zwei Fälle:
            \begin{enumerate}
                \item die Teilmengen haben verschiedene Ränge\\
                    dann fügen wir die Teilmenge mit kleinerem Rang an
                    diejenige mit größerem Rang an
                \item die Teilmengen haben gleichen Rang\\
                    dann ist es egal, welche Teilmenge wir anhängen, doch
                    wir erhöhen den Rang der Menge, an die angehängt 
                    wird, um 1
            \end{enumerate}
    \end{itemize}
    Damit beschränken wir die Höhe aller Baumstrukturen durch $\log n$.
\end{frame}
\begin{frame}{Pfadkompression}
    Bei der Verschmelzung von Teilmengen können trotzdem noch lange
    Pfade entstehen, wenn auf ein Eltern-Element nur wenige Kinder 
    kommen. Um die Höhe der Bäume weiter zu reduzieren, setzen wir nun
    bei der Operation find an:\\[.5em]
    Wann immer wir find aufrufen, biegen wir die Eltern-Zeiger aller 
    iterierten Elemente direkt auf den gefundenen Repräsentanten um, 
    sodass alle folgenden find-Aufrufe auf eines dieser Elemente in 
    \bigO{1} ablaufen.
\end{frame}
\begin{frame}{Optimierte Laufzeiten}
    Mit diesen beiden Optimierungen bekommen wir erfreuliche Laufzeiten:
    \begin{itemize}
        \item $m \cdot find + n \cdot union \in$ \bigO{m \cdot 
            \alpha_T(m,n)}
        \item $\alpha_T$ ist hierbei die inverse Ackermann-Funktion und
            damit unglaublich klein
    \end{itemize}
\end{frame}
\end{document}
