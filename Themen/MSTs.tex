\documentclass{beamer}
\usetheme{metropolis}
\usepackage{textcomp}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{ stmaryrd }
\input{../makros/math_makros.sty}

\setbeamercolor{block title}{bg=gray!30}
\setbeamercolor{block body}{bg=gray!20}

\title{Algorithmen I Tutorium}
\date{\today}
\author{Henriette Färber}
\institute{Tutorium 10}

\begin{document}
\maketitle
\section{Minimale Spannbäume}
\begin{frame}{Begriffsklärung}
    \begin{block}{Spannbaum (Spanning Tree)}
        Ein Spannbaum eines ungerichteten Graphen G = (V,E) ist ein 
        Teilgraph, der alle Knoten von G enthält, aber ein Baum ist.
    \end{block}
    \begin{block}{Minimum Spanning Tree (MST)}
        Ein minimaler Spannbaum eines ungerichteten, gewichteten 
        Graphen G = (V,E) ist ein Spannbaum T = (V, $E_T$), wenn die 
        Summe der Gewichte aller Kanten aus $E_T$ minimal ist, d.h.\\
        \[\sum_{e \in E_T}{c(e)} \leq \sum_{e \in E'}{c(e)}\]\\
        für jeden weiteren Spannbaum T' = (V, E').
    \end{block}
\end{frame}
\begin{frame}{Begriffsklärung}
    Welche Eigenschaft muss ein Graph besitzen, damit er einen 
    Spannbaum haben kann?\\[.5em]
    \pause
    Er muss zusammenhängend sein.\\[1em] 
    In unzusammenhängenden Graphen kann man in jeder 
    Zusammenhangskomponente einen Spannbaum finden und nennt das
    Gesamtkonstrukt dann Spannwald.\\[1em]
    Minimale Spannbäume sind nützlich, um in einem Modell ein 
    zusammenhängendes Netzwerk mit minimalen Kosten zu finden. Man kann
    sie auch zum Finden einer approximativen Lösung für das TSP 
    verwenden.
\end{frame}
\begin{frame}{Aufgabe}
    Findet einen MST in folgendem Graphen:
    \begin{figure}[htb]
        \includegraphics[width=0.6\textwidth]{../illustrations/mst/find_mst.pdf}
    \end{figure}
    Ist dieser MST eindeutig?
\end{frame}
\begin{frame}{Eigenschaften von MSTs}
    Welche Kanten sind Teil eines MST?
    \begin{block}{Kreiseigenschaft}
        Die schwerste Kante auf einem Kreis wird für einen MST nicht
        benötigt.
    \end{block}
    \begin{block}{Schnitt}
        Sei G = (V,E) ein Graph und $S \subset V$ eine echte Teilmenge
        von Knoten. Ein Schnitt C von G ist definiert als\\
        C := \{ (v,w) $\in$ E : $v \in S \land w \in V \backslash S$ \} 
    \end{block}
    \begin{block}{Schnitteigenschaft}
        Die leichteste Kante eines Schnitts kann in einem MST verwendet 
        werden.
    \end{block}
\end{frame}
\begin{frame}{Aufgabe}
    Ist die Schnitteigenschaft im folgenden Graphen erfüllt?
    \begin{figure}[htb]
        \includegraphics[width=0.6\textwidth]{../illustrations/mst/cut_prop.pdf}
    \end{figure}
    Beweist die Kreiseigenschaft!
\end{frame}
\begin{frame}{Lösung}
    Beweis der Kreiseigenschaft:\\[1em]
    Seien G = (V,E) ein ungerichteter gewichteter Graph und\\
    T = (V,$E_T$) ein MST von G.\\
    Angenommen, T enthält die schwerste Kante e auf einem Kreis C in G.
    Wie wählen e' $\in$ C mit e' $\notin$ T. Dann muss c(e') $\leq$ c(e)
    gelten.\\
    Somit ist T' = (V, $E'_T := E_T \backslash \{e\} \cup \{e'\}$) 
    ebenfalls ein Spannbaum von G und\\
    \[\sum_{e \in E_T}{c(e)} \leq \sum_{e \in E'_T}{c(e)}\]
    $\lightning$ Widerspruch zu: T ist minimal
\end{frame}
\begin{frame}{Finden von MST}
    Wir lernen zwei einfache Algorithmen kennen, die uns das Finden von
    MSTs ermöglichen:
    \begin{itemize}
        \item Jarník-Prim: basiert auf der Schnitteigenschaft
        \item Kruskal: basiert auf der Kreiseigenschaft
    \end{itemize}
\end{frame}
\section{Der Jarník-Prim-Algorithmus}
\begin{frame}{Idee}
    \begin{itemize}
        \item basierend auf der Schnitteigenschaft
        \item Vorgehen:
            \begin{enumerate}
                \item wähle $s \in V$ beliebig, setze S \< \{s\} und 
                    $E_T \gets \emptyset$
                \item finde leichteste Kante in Schnitt S, füge sie $E_T$
                    hinzu
                \item setze $S \gets S \cup \{v\}$
                \item wiederhole Schritte 2 und 3 bis S = V
            \end{enumerate}
    \end{itemize}
\end{frame}
\begin{frame}{Pseudocode}
    \begin{algorithm}[H]
        \begin{algorithmic}[1]
            \scriptsize
            \Function{jarnikPrim}{G=(V,E): Graph} : [Edge]
            \State s \< random(V) : Vertex 
            \State PQ \< \{s\} : PriorityQueue
            \State d \< $<\infty, \ldots, \infty>$: [$\mathbb{N}_0 \cup
            \{\infty\}$, \norm{V}] 
            \State parent \< $<\bot, \ldots, \bot>$: [Vertice / $\bot$,
            $|V|$]
            \State parent[s] \< s, d[s] \< 0
            \While{PQ $\neq \emptyset$}
            \State v \< PQ.deleteMin()
            \State d[v] \< 0
            \For{\textbf{each} e = (v,w) \textbf{in} E}
            \If{c(e) $<$ d[w]}
            \State d[w] \< c(e), p[w] \< v
            \If{w $\in$ PQ} PQ.decreaseKey(w)
            \Else PQ \< PQ $\cup$ \{w\}
            \EndIf
            \EndIf
            \EndFor
            \EndWhile
            \State \textbf{return} \{(v,p[v]) $|\ v \in V\backslash \{s\}$\}
            \EndFunction
        \end{algorithmic}
    \end{algorithm}
\end{frame}
\begin{frame}{Visualisierung}
    \begin{figure}[htb]
        \includegraphics[width=0.6\textwidth]{../illustrations/mst/jp_00.pdf}
    \end{figure}
\end{frame}
\begin{frame}{Visualisierung}
    \begin{figure}[htb]
        \includegraphics[width=0.6\textwidth]{../illustrations/mst/jp_01.pdf}
    \end{figure}
\end{frame}
\begin{frame}{Visualisierung}
    \begin{figure}[htb]
        \includegraphics[width=0.6\textwidth]{../illustrations/mst/jp_02.pdf}
    \end{figure}
\end{frame}
\begin{frame}{Visualisierung}
    \begin{figure}[htb]
        \includegraphics[width=0.6\textwidth]{../illustrations/mst/jp_03.pdf}
    \end{figure}
\end{frame}
\begin{frame}{Visualisierung}
    \begin{figure}[htb]
        \includegraphics[width=0.6\textwidth]{../illustrations/mst/jp_04.pdf}
    \end{figure}
\end{frame}
\begin{frame}{Visualisierung}
    \begin{figure}[htb]
        \includegraphics[width=0.6\textwidth]{../illustrations/mst/jp_05.pdf}
    \end{figure}
\end{frame}
\begin{frame}{Visualisierung}
    \begin{figure}[htb]
        \includegraphics[width=0.6\textwidth]{../illustrations/mst/jp_06.pdf}
    \end{figure}
\end{frame}
\begin{frame}{Aufgabe}
    Führt den Jarnik-Prim-Algorithmus auf dem folgenden Graphen aus, 
    notiert dabei die MST-Kanten in der Reihenfolge, wie sie zum MST
    hinzugefügt werden.
    \begin{figure}[htb]
        \includegraphics[width=0.7\textwidth]{../illustrations/mst/jp_exe.pdf}
    \end{figure}
\end{frame}
\begin{frame}{Lösung}
    \begin{figure}[htb]
        \includegraphics[width=0.7\textwidth]{../illustrations/mst/jp_exe.pdf}
    \end{figure}
\end{frame}
\section{Der Kruskal-Algorithmus}
\begin{frame}{Idee}
    \begin{itemize}
        \item basierend auf der Kreiseigenschaft
        \item Vorgehen:
            \begin{enumerate}
                \item sortiere Kantenmenge E aufsteigend nach Gewicht
                \item setze S \< $\emptyset$ und $E_T \gets \emptyset$
                \item betrachte Kanten in aufsteigender Reihenfolge:\\
                    bildet e = (v,w) $\in$ E keinen Kreis in G' = (S, 
                    $E_T$), setze\\
                    S \< $S \cup \{v,w\}$ und $E_T \gets E_T \cup \{e\}$
            \end{enumerate}
    \end{itemize}
\end{frame}
\begin{frame}{Pseudocode}
    \begin{algorithm}[H]
        \begin{algorithmic}[1]
            \Function{Kruskal}{G = (V,E): Graph}: [Edge]
            \State MST \< $\emptyset$ : [Edge]
            \State sortAscending(E)\\
            \Comment passenden ALgorithmus wählen, nach Gewicht
            \For{\textbf{each} e = (v,w) \textbf{in} E}
            \If{find(v) $\neq$ find(w)}
            \Comment union-find
            \State union(u,v)
            \State MST \< MST $\cup$ \{e\}
            \EndIf
            \EndFor
            \State \textbf{return} MST
            \EndFunction
        \end{algorithmic}
    \end{algorithm}
\end{frame}
\begin{frame}{Visualisierung}
    \begin{figure}[htb]
        \includegraphics[width=0.6\textwidth]{../illustrations/mst/kru_00.pdf}
    \end{figure}
\end{frame}
\begin{frame}{Visualisierung}
    \begin{figure}[htb]
        \includegraphics[width=0.6\textwidth]{../illustrations/mst/kru_01.pdf}
    \end{figure}
\end{frame}
\begin{frame}{Visualisierung}
    \begin{figure}[htb]
        \includegraphics[width=0.6\textwidth]{../illustrations/mst/kru_02.pdf}
    \end{figure}
\end{frame}
\begin{frame}{Visualisierung}
    \begin{figure}[htb]
        \includegraphics[width=0.6\textwidth]{../illustrations/mst/kru_03.pdf}
    \end{figure}
\end{frame}
\begin{frame}{Visualisierung}
    \begin{figure}[htb]
        \includegraphics[width=0.6\textwidth]{../illustrations/mst/kru_04.pdf}
    \end{figure}
\end{frame}
\begin{frame}{Aufgabe}
    Führt den Kruskal-Algorithmus auf dem folgenden Graphen aus, 
    notiert dabei die MST-Kanten in der Reihenfolge, wie sie zum MST
    hinzugefügt werden.
    \begin{figure}[htb]
        \includegraphics[width=0.7\textwidth]{../illustrations/mst/jp_exe.pdf}
    \end{figure}
\end{frame}
\begin{frame}{Lösung}
    \begin{figure}[htb]
        \includegraphics[width=0.7\textwidth]{../illustrations/mst/jp_exe.pdf}
    \end{figure}
\end{frame}
\begin{frame}{Laufzeiten}
    Sei für G = (V,E) $|V| = n$ und $|E| = m$.\\[0.5em]
    Die Laufzeit vom Jarník-Prim-Algorithmus hängt vor allem auf der Art
    der PriorityQueue ab, die wir verwenden. In diese wird jeder Knoten
    je einmal eingefügt, ggf. (mehrmals) geupdated und wieder gelöscht.
    Wird für die Implementierung ein Fibonnaci-Heap verwendet, ist die
    Laufzeit optimal und in\\
    \bigO{m + n \cdot \log n}\\[1em]
    Die Laufzeit vom Kruskal-Algorithmus hängt vor allen Dingen an der
    Laufzeit des Sortieralgorithmus ab und liegt allgemein bei\\
    \bigO{sort + n}\\[.5em]
    (Das bedeutet auch, dass bei bestimmten Zusicherungen von 
    Kanteneigenschaften hier einiges optimiert werden kann.)
\end{frame}
\end{document}
