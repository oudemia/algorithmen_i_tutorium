\documentclass{beamer}
\usetheme{metropolis}
\usepackage{textcomp}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{amsmath}
\input{../makros/math_makros.sty}

\setbeamercolor{block title}{bg=gray!30}
\setbeamercolor{block body}{bg=gray!20}

\title{Algorithmen I Tutorium}
\date{\today}
\author{Henriette Färber}
\institute{Tutorium 10}

\begin{document}
\maketitle
\section{Greedy Algorithmen}
\begin{frame}{Was bedeutet \textit{greedy}?}
    \textit{Greedy} bedeutet nichts anderes als \textit{gierig}.
    Im Kontext von Algorithmen bedeutet das, dass wir einmal berechnete
    Ergebnisse stets behalten und nicht mehr verändern.\\[.5em]
    (Wenn man den Begriff so weit strapazieren möchte, kann man 
    \textit{gierig} auch als geizig mit Laufzeit oder Ressourcen 
    auffassen)
\end{frame}
\begin{frame}{Begriffsklärung}
    \vspace{.5em}
    \begin{block}{Greedy Algorithmus}
        Ein Algorithmus wird als \textit{greedy} bezeichnet, wenn er
        in jeder Situation eine lokal optimale Entscheidung trifft.
    \end{block}
    Das bedeutet, dass er zu untersuchende Elemente in einer bestimmten
    Reihenfolge betrachtet und eine Entscheidung, nachdem er sie
    getroffen hat, nicht mehr revidiert.
\end{frame}
\begin{frame}{Beispiele}
    Wir haben im Laufe der Vorlesung bereits einige Greedy Algorithmen
    kennengelernt:
    \pause
    \begin{itemize}
        \item selectionSort
            \pause
        \item Dijkstra
            \pause
        \item Jarník-Prim
            \pause
        \item Kruskal
    \end{itemize}
    Was haben diese Algorithmen gemeinsam?\\
    \pause
    Sie liefern optimale Lösungen.
\end{frame}
\begin{frame}{Aufgabe}
    Dr. Meta hat im Zuge seiner Sparmaßnahmen eine neue Strategie 
    entwickelt: Er lässt seine Minions Passanten das Geld aus der 
    Hosentasche stehlen.\\
    Da dies bei seinen Kollegen als unter der Würde eines Superschurken
    gilt, möchte er bei den gemeinsamen Abendessen in der Cafeteria 
    keinen Verdacht erregen und beschließt, stets mit so wenig Münzen
    wie möglich zu zahlen. (Bsp.: 20ct + 5ct für 25ct Betrag)
\end{frame}
\begin{frame}{Aufgabe}
    Wir nehmen an, dass Dr. Meta stets nur ganzzahlige Werte zahlen muss.
    Welchen wert muss die niedrigstwertige Münze auf jeden Fall haben?
    \\[1em]\pause
    Dr. Meta hat Münzen mit den folgenden Werten dabei: 1ct, 5ct, 10ct,
    50ct. Wie zahlt er am Besten eine Rechnung von 5€ 19ct?\\[1em]\pause
    Wie könnte ein Algorithmus aussehen, der, gegeben einer Menge
    von Münzwerten und einem zu zahlenden Betrag, diese minimale
    Menge von Münzen findet? Wir gehen dabei davon aus, dass Dr. Meta 
    eine unendliche Menge jedes Münzwertes angehäuft hat.
\end{frame}
\begin{frame}{Aufgabe}
    \begin{itemize}
        \item gegeben: $m_1,\ldots,m_k$ Münzwerte mit 
            \begin{itemize}
                \item $m_1 = 1$
                \item $\forall i \in \{1,\ldots, k-1\}: m_i < m_{i+1}$
            \end{itemize}
        \item außerdem: der zu zahlende Betrag $K \in \mathbb{N}_0$
    \end{itemize}
    \pause
    Idee:
    \begin{enumerate}
        \item Bestimme das größte $m_i$ so, dass $m_i \leq K$
        \item Setze $K = K - m_i$, merke $m_i$
        \item Falls K = 0, kehre zurück, ansonsten beginne wieder bei 1
    \end{enumerate}
    \pause
    \textbf{Behauptung}: Dieser Algorithmus liefert ein optimales 
    Ergebnis.
\end{frame}
\begin{frame}{Optimalitätsbeweis}
    \begin{block}{Minimalität}
        Wir nennen eine Menge M von Münzen, die zusammen den Wert K 
        bilden, minimal, wenn sie für alle $i \in \{1,\ldots, k-1\}$ 
        weniger als $\frac{m_{i+1}}{m_i}$ Münzen vom Wert $m_i$ enthält.
    \end{block}
    Wenn eine Münzmenge also nach dieser Definition minimal ist, gibt
    es keine anderen mengen, die ebenfalls Betrag K bilden, aber weniger
    Münzen beinhalten.
\end{frame}
\begin{frame}{Optimalitätsbeweis}
    Seien $m_1,\ldots,m_k$ die Menge an möglichen Münzwerten und $i\in
    \{1,\ldots,k\}$. Sei M eine Menge von Münzen, die keine Münzen von 
    Wert $m_i$ oder höher enthalte. Der maximale Wert von M ist
    \begin{gather*}
    (\frac{m_2}{m_1} - 1)\cdot m_1 + (\frac{m_3}{m_2} - 1)\cdot m_2 +
        \dotsb + (\frac{m_i}{m_{i-1}} - 1)\cdot m_{i-1})\\
        = (m_2 - m_1) + (m_3 - m_2) + \dotsb + (m_i - m_{i-1})\\
        = m_i - m_1\\
        = m_i - 1
    \end{gather*}
\end{frame}
\begin{frame}{Optimalitätsbeweis}
    Wir beweisen nun die Optimalität des Algorithmus, den wir vorher
    skizziert haben, indem wir beweisen, dass er immer die richtigen
    Münzen auswählt:\\[.5em]
    Wir betrachten nun K mit $m_i \leq K < m_{i+1}$. Eine optimale Menge
    M' muss im Sinne der vorigen Definition minimal sein.\\
    Wegen $m_i \leq K$ muss M' mindestens eine Münze von Wert $m_i$ oder
    höher enthalten. Wegen $K < m_{i+1}$ kann S keine Münze mit Wert 
    $m_{i+1}$ oder höher enthalten. Somit enthält M' also eine Münze
    mit Wert $m_i$.\\[.5em]
    Dieses Argument lässt sich sukzessive fortsetzen, indem wir nun 
    eine Münze mit Wert $m_i$ aus  M' entfernen und $K = K - m_i$ setzen.
    Damit finden wir die Münzen einer optimalen Lösung in genau der
    Reihenfolge, in der sie der Algorithmus hinzufügt.
\end{frame}
\begin{frame}[t]{Coin Change Problem}
    Hurra! Dr. Meta kann also nun weiterhin seine Minions auf Passanten
    loslassen, ohne sich vor seinen Kollegen zu blamieren.\\
    Fragt sich noch: Wie schnell kann Dr. Meta seine minimale
    Münzmenge finden, sobald er einen Preis erfährt?
\end{frame}
\end{document}
