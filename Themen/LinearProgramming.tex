\documentclass{beamer}
\usetheme{metropolis}
\usepackage{textcomp}
\usepackage{algorithm}
\usepackage{algpseudocode}
\input{../makros/math_makros.sty}

\setbeamercolor{block title}{bg=gray!30}
\setbeamercolor{block body}{bg=gray!20}

\title{Algorithmen I Tutorium}
\date{\today}
\author{Henriette Färber}
\institute{Tutorium 10}

\begin{document}
\maketitle
\section{Lineare Programmierung}
\begin{frame}{Lineare Optimierungsprobleme}
    Wir haben bereits reguläre Optimierungsprobleme kennengelernt. 
    Nun schränken wir uns auf lineare Optimierungsprobleme ein.
    \begin{block}{Lineares Optimierungsproblem}
        Ein lineares Optimierungsproblem besteht aus:
        \begin{itemize}
            \item einer lineare Zielfunktion
            \item linearen Nebenbedingungen (Restriktionen)
            \item der Aufgabe, den Wert der Zielfunktion zu maximieren
                oder minimieren
        \end{itemize}
    \end{block}
\end{frame}
\begin{frame}{Notation}
    Wir notieren ein lineares Optimierungsproblem in der folgenden Form:
    \begin{itemize}
        \item gegeben: 
            \begin{itemize}
                \item A $\in \mathbb{R}^{m \times n}$
                \item c $\in \mathbb{R}^{n}$
                \item b $\in \mathbb{R}^{m}$ 
                \item x $\in \mathbb{R}^{n}$, den Vektor mit 
                    Entscheidungsvariablen
            \end{itemize}
        \item gesucht: Belegungen von Komponenten von x so, dass 
            \begin{itemize}
                \item $c \cdot x$ optimal
                \item $\forall j \in \{1,\ldots,m\}: \sum_{i = 1}^n{
                    (a_{ij} \cdot x_i)} \leq b_j$
            \end{itemize}
    \end{itemize}
\end{frame}
\begin{frame}{Integer Linear Programming}
    Beim Integer Linear Programming stellen sich uns Probleme mit der
    gleichen Struktur, aber mit dem Unterschied, dass eine Lösung nur
    dann gültig ist, wenn alle Belegungen der Entscheidungsvariablen
    ganzzahlig sind, d.h. wenn x $\in \mathbb{Z}^{n}$.
\end{frame}
\begin{frame}{Anmerkung}
    Wir werden uns in Algorithmen I nicht mit dem Lösen von Problemen
    mit Hilfe von (Integer) Linear Programming beschäftigen, sondern nur
    mit dem Aufstellen der Probleme.\\[1em]
    Für diejenigen, die gerne mehr erfahren wollen, bietet sich das
    Nebenfach Operations Research an. Aus dem Lehrbuch, dass von 
    S. Nickel, O. Stein und K. Waldmann zu der Einführungsveranstaltung
    verfasst wurde\\ (Operations Research, 2. Auflage, Springer, Berlin 
    Heidelberg) stammt auch das folgende Beispiel.
\end{frame}
\begin{frame}{Modellierung}
    Betrachtet die folgende Situation: Ein Unternehmen stellt zwei 
    Produkte her: $P_1 und P_2$, wobei $P_1$ 3€ Gewinn pro produzierter
    Mengeneinheit (ME) bringt und $P_2$ sogar 4€.\\
    Ziel des Unternehmens ist es, möglichst viel Gewinn zu machen. Dabei
    sind die Ressourcenverbrauche der Produkte zu beachten, die sich 
    wie folgt zusammensetzen:
    \begin{center}
        \begin{tabular}{c | c | c}
            \textbf{Ressource} & $P_1$ & $P_2$\\
            \hline \hline
            \textbf{Maschinenstunden} & 3h & 2h\\
            \hline
            \textbf{Rohstoff} & 5ME & 10ME\\
            \hline
            \textbf{Arbeitsstunden} & 0h & 0.5h
        \end{tabular}
    \end{center}
    Im betrachteten Zeitraum stehen nur 1200 Maschinenstunden, 3000ME
    Rohstoffe und 125 Arbeitsstunden zur Verfügung.
\end{frame}
\begin{frame}{Modellierung}
    Wir benötigen zwei Entscheidungsvariablen, d.h. x = $(x_1\ x_2)$,
    wobei $x_i$ die Menge bezeichnet, die von $P_i$ produziert werden 
    soll. Es gilt x $\in \mathbb{Z}^2$.\\
    Es handelt sich um ein Maximierungsproblem. Maximiert werden soll der
    Gewinn, der sich aus der Summe der Gewinne für die beiden Produkte
    zusammensetzt. Damit ist die Zielfunktion gegeben als\\[0.5em]
    $f(x) = 3x_1 + 4x_2$.\\[.5em]
\end{frame}
\begin{frame}{Modellierung}
    Die Restriktionen ergeben sich aus den Ressourceneinschränkungen:\\
    \begin{gather*}
        3x_1 + 2x_2 \leq 1200\\
        5x_1 + 10x_2 \leq 3000\\
        0.5x_2 \leq 125\\
        x_1, x_2 \geq 0
    \end{gather*}
    Die letzte Restriktion, die sogenannten Nichtnegativitätsbedingungen,
    sind wichtig und dürfen nicht vergessen werden!
\end{frame}
\begin{frame}{Aufgabe}
    Dr. Meta lässt für die Konstruktion seiner schurkenhaften Maschinen
    zwei Gruben, in denen seine Minions Erz abbauen.\\
    Es werden drei verschiedene Erzarten gefördert: feines, mittleres und
    grobes Erz.\\
    Um seine Produktion stets aufecht erhalten zu können, benötigt Dr.
    Meta pro Woche mindestens 12t grob-, 8t mittel- und 24t feinkörniges
    Erz.\\
    Die Betriebskosten belaufen sich für Grube 1 auf 20000€ und bei 
    Grube 2 auf 1600€. In Grube 1 werden pro Tag 6t grob- 2t mittel- und
    4t feinkörniges Erz gefördert, Grube 2 bringt täglich 2t grob-, 2t
    mittel- und 12t feinkörniges Erz.\\[.5em]
    Formuliert ein lineares Optimierungsproblemproblem, welches diese
    Situation modelliert!
\end{frame}
\begin{frame}{Lösung}
    \begin{itemize}
        \item Entscheidungsvariablen = \pause
            Anzahl der Arbeitstage in den 
            Gruben $\rightsquigarrow x = (x_1\ x_2) \in \mathbb{Z}$
        \item Zielfunktion: \pause $f(x) = 20000x_1 + 16000x_2$
        \item wir wollen die Zielfunktion \pause minimieren
        \item Restriktionen:
    \end{itemize}
    \pause
    \begin{gather*}
        6x_1 + 2x_2 \leq 12\\
        2x_1 + 2_2 \leq 8\\
        4x_1 + 12x_2 \leq 24\\
        x_1, x_2 \leq 7\\
        x_1, x_2 \geq 0
    \end{gather*}
\end{frame}
\end{document}
